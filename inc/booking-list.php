<?php
if (!class_exists('WP_List_Table')) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}
class Booking_list extends WP_List_Table {

    var $_column_headers;
    var $items;

    /**
     * set data for representation
     */
    function prepare_items() {
        global $wpdb;
        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();
        
        
        $date=apply_filters('get_saturday_week_date',0);

        if(isset($_REQUEST['subscription_date']) && $_REQUEST['subscription_date']){
            $date=$_REQUEST['subscription_date'];
        }
        $extra_condition = "";

        $this->_column_headers = array($columns, $hidden, $sortable);
        $query = "select * from ".$wpdb->base_prefix."user_booking";

//        $orderby = !empty($_GET["orderby"]) ? $_GET["orderby"] : 'ASC';
//        $order = !empty($_GET["order"]) ? $_GET["order"] : '';
//        if (!empty($orderby) && !empty($order)) {
//            $query .= ' ORDER BY ' . $orderby . ' ' . $order;
//        } else {
//            $query .= ' ORDER BY  id DESC';
//        }

        $totalitems = $wpdb->query($query);
        $perpage = $this->get_items_per_page();
        $paged = !empty($_GET["paged"]) ? $_GET["paged"] : '';
        if (empty($paged) || !is_numeric($paged) || $paged <= 0) {
            $paged = 1;
        }
        $totalpages = ceil($totalitems / $perpage);
        if (!empty($paged) && !empty($perpage)) {
            $offset = ($paged - 1) * $perpage;
            $query .= ' LIMIT ' . (int) $offset . ',' . (int) $perpage;
        }

        //The pagination links are automatically built according to those parameters
        /* -- Register the Columns -- */

        $this->set_pagination_args(array(
            "total_items" => $totalitems,
            "total_pages" => $totalpages,
            "per_page" => $perpage,
        ));

        $this->items = $wpdb->get_results($query);
    }

    /**
     *  native table columns headers 
     */
    function get_columns() {
        $columns = array(            
            'first_name' =>'First Name',
            'last_name' =>'Last Name',
            'email' =>'Email',
            'phone_no' =>'Phone No',
            'vehicle_type' =>'Vehicle Type',
            'vehicle' =>'Vehicle',
            'price' =>'Price',
            'booking_status' =>'Status',
            'action' =>'Change Status'
        );
        return $columns;
    }
    
    
    /**
     * get first name
     */
    function column_first_name($item) {
          global $wpdb;
      
      echo $item->first_name;
    }
    
    /**
     * get last name
     */
    function column_last_name($item) {
          global $wpdb;
      
      echo $item->last_name;
    }
    
     /**
     * get email
     */
    function column_email($item) {
          global $wpdb;
      
      echo $item->email;
    }
    
     /**
     * get email
     */
    function column_phone_no($item) {
          global $wpdb;
      
      echo $item->phone_no;
    }
    
    
      /**
     * get vehicle_type
     */
    function column_vehicle_type($item) {
          global $wpdb;
      
      
      $tr= get_the_category_by_ID( $item->vehicle_type );
      if(!is_object($tr)){
          echo $tr;
      }
     
    }
    
       
      /**
     * get vehicle
     */
    function column_vehicle($item) {
          global $wpdb;
      
      
      echo get_the_title($item->vehicle);
    }
    
      /**
     * get price
     */
    function column_price($item) {
          global $wpdb;
      
      echo $item->price;
    }
     

       /**
     * get price
     */
    function column_booking_status($item) {
          global $wpdb;
      
      echo $item->booking_status;
    }
     
    
    function column_action($item) {
        echo "<a href='?page=change_booking_option&post_type=vehicle&action=view&id=".$item->id."' >View</a>";
       // echo '<a href="?page=teams&action=delete&id=' . $item->id . '" title="Delete Team Member" onclick="return confirm(\'Are you sure you to want delete?\')">Delete</a>';
    }

    /**
     *
     * sort data according to columns wise
     */
    function get_sortable_columns() {
        $sortable_columns = array(
            'size_name' => array('price', true)
        );
        return $sortable_columns;
    }

    /**
     * set pagination value record per page
     */
    public function get_items_per_page($option = '', $default = 40) {
        $perpage = 40;
        return $perpage;
    }

    public function no_items() {
        _e('No records found');
    }

}