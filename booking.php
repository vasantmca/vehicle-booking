<?php
/**
  Plugin Name: Booking System
  Plugin URI: http://google.com
  Description: The plugin is basically developed to manage booking.
  Version: 1.0.0
  Author: Vasant Kumar
  Author URI: http://google.com
 */


$obj=new Booking();
define("ADMIN_EMAIL", '');
define("ADMIN_NAME", '');
class Booking {

    public $slug = 'vehicle'; 
    public $title = 'Vehicle';
   
    
    public function __construct() {
       
        
        // Actions
        add_action('init', array($this, 'coustom_post')); // register custom post type
        add_action('init', array($this, 'coustom_post_category')); // register custom post type category
        add_shortcode('booking-form', array($this, 'bookingForm'));
        register_activation_hook( __FILE__, array($this,'activationBookingPlugin') );
        
        add_action('wp_ajax_get_vehicles_price', array($this, 'getVehiclesPrice'));
        add_action('wp_ajax_nopriv_get_vehicles_price', array($this, 'getVehiclesPrice'));
        add_action('admin_menu', array($this, 'addAdminPage'));
        
        add_action( 'add_meta_boxes', array($this,'addVehiclesMetaboxes' ));

       add_action( 'save_post', array($this,'saveVehiclePrice'), 10, 2 );

    }

        

  
      
    public function coustom_post() {
        // allowed field in admin section
        
        $supports = array('title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments', '');
        $labels = array(
            'name' => $this->title,
            'singular_name' => $this->title,
            'menu_name' => $this->title,
            'name_admin_bar' => $this->title,
            'add_new' => 'Add ' . $this->title,
            'add_new_item' => 'Add New ' . $this->title,
            'new_item' => 'New ' . $this->title,
            'edit_item' => 'Edit ' . $this->title,
            'view_item' => 'View ' . $this->title,
            'all_items' => 'All ' . $this->title,
            'search_items' => 'Search ' . $this->title,
            'parent_item_colon' => 'Parent ' . $this->title . ':',
            'not_found' => 'No ' . $this->title . ' found.',
            'not_found_in_trash' => 'No ' . $this->title . ' found in Trash.'
        );
        $args = array(
            'labels' => $labels,
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'query_var' => true,
            'rewrite' => array('slug' => $this->slug, 'with_front' => true),
            'menu_icon' => 'dashicons-admin-post',
            'capability_type' => 'post',
            'has_archive' => true,
            'hierarchical' => false,
            'menu_position' => 8,
            'supports' => $supports
        );
        register_post_type($this->slug, $args);
    }

    public function coustom_post_category() {
        register_taxonomy(
            'vehicle-type', 
            'vehicle', 
            array(
                'labels' => array(
                    'name' => 'Vehicle Type',
                    'add_new_item' => 'Add New Vehicle Type' ,
                    'new_item_name' => "New Vehicle Type" , 
                ),
                'show_ui' => true,
                'hierarchical' => true,
                'parent_item'  => null,
                'parent_item_colon' => null,
                //'show_tagcloud' => true,
            )
        );
    }
    public function coustom_post_tag() {
        register_taxonomy(
            $this->slug.'_tag', 
            $this->slug, 
            array(
                'labels' => array(
                    'name' => $this->title. ' Tag',
                    'add_new_item' => 'Add New ' . $this->title . ' Tag',
                    'new_item_name' => "New " . $this->title . ' Tag'
                ),
                'show_ui' => true,
                'hierarchical' => null,
                'parent_item'  => null,
                'parent_item_colon' => null,
                //'show_tagcloud' => true,
            )
        );
    }
    
    
    public function addAdminPage(){
        add_submenu_page('edit.php?post_type=vehicle', 'Booking', 'Booking', 'manage_options', 'user_booking_list', array($this, 'bookingList'), 'dashicons-menu');
        add_submenu_page('edit.php?post_type=vehicle', '', '', 'manage_options', 'change_booking_option', array($this, 'changeBookingStatus'), 'dashicons-menu');
    }

    public function bookingForm(){ 
      global $wpdb;
     
      
      $submit_flag=0;
     if(isset($_POST['first_name']) && $_POST['first_name']!=''){
         $booking_array=array();
         $booking_array['first_name']=$_POST['first_name'];
         $booking_array['last_name']=$_POST['last_name'];
         $booking_array['email']=$_POST['email'];
         $booking_array['phone_no']=$_POST['phoneno'];
         $booking_array['vehicle_type']=$_POST['vehicle_type'];
         $booking_array['vehicle']=$_POST['vehicle_data'];
         $booking_array['price']=$_POST['price'];
         $booking_array['message']=$_POST['message'];
         $booking_array['booking_status']='Pending';
         $wpdb->insert($wpdb->base_prefix."user_booking",$booking_array);
         $submit_flag=1;
         // Email code
         $tr= get_the_category_by_ID( $booking_array['vehicle_type'] );
          $vehicle_type='';
      if(!is_object($tr)){
          $vehicle_type= $tr;
      }
         $body="";
         $body.="Name: ".$booking_array['first_name']." ".$booking_array['last_name']."/n";
         $body.="Email: ".$booking_array['email']."/n";
         $body.="Phone No: ".$booking_array['phone_no']."/n";          
         $body.="Vehicle Type: ".$vehicle_type."/n";
         $body.="Vehicle : ".get_the_title($booking_array['vehicle'])."/n";
         $body.="Price: ".$booking_array['price']."/n";
         $body.="Message: ".$booking_array['message']."/n";
         // For Admin
         $this->sendEMail(ADMIN_EMAIL, 'New request from customer', $body);
         $body .="Status: Pending";
         // For User
         $this->sendEMail($booking_array['email'], 'New request', $body);
     }
      
      $vehicle_types = get_terms( array(
    'taxonomy' => 'vehicle-type',
    'hide_empty' => false,
) );
      //print_r($vehicle_types);
      //$all_post = get_posts(array('post_type' => 'vehicle','category'=>[30]));
      $all_post = get_posts(array('post_type' => 'vehicle'));
      //print_r($all_post);
      $args=array("category"=>29,'post_type'=>'vehicle','numberposts'=>-1,    'post_status' => 'publish',
);
      
      $args = array( 'numberposts' => 10, 'orderby' => 'date');
//$carousel = get_posts($args);
//print_r($carousel);

?>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<?php if($submit_flag){ ?>
<div class="alert alert-success" role="alert">
  Information has been saved
</div>
<?php } ?>
<form name="booking-form" method="post"  action="?/" autocomplete="off">
  <div class="form-group">
    <label for="exampleInputEmail1">First Name</label>
    <input name="first_name" type="text" class="form-control" id="first_name" aria-describedby="emailHelp" placeholder="Enter first name" required="">
    
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Last Name</label>
    <input name="last_name" type="text" class="form-control" id="last_name" aria-describedby="emailHelp" placeholder="Enter last name" required="">
    
  </div>
    
    <div class="form-group">
    <label for="exampleInputEmail1">Email</label>
    <input name="email" type="email" class="form-control" id="last_name" aria-describedby="emailHelp" placeholder="Enter email" required="">
    
  </div>
    <div class="form-group">
    <label for="exampleInputEmail1">Phone No</label>
    <input name="phoneno" type="text" class="form-control" id="last_name" aria-describedby="emailHelp" placeholder="Enter phone no" required="">
    
  </div>
    
    <div class="form-group">
    <label for="exampleInputEmail1">Select vehicle type</label>
    <select  class="form-control" name="vehicle_type" id="vehicle_type" required="">
        <option value="0">Select vehicle type</option>
        <?php 
        foreach( $vehicle_types as $cat ) { ?>
   
     <option value="<?php echo $cat->term_id;?>"><?php echo $cat->name;?></option>
   <?php }?>
        </select>
  </div>
    
      <div class="form-group">
    <label for="exampleInputEmail1">Select vehicle</label>
    <select  class="form-control"  name="vehicle_data" id="vehicle" required="" onchange="return getPriceData();">
        <option value="0">Select vehicle </option>
        <?php 
        foreach( $all_post as $vehicle ) { ?>
   
     <option value="<?php echo $vehicle->ID;?>"><?php echo $vehicle->post_title;?></option>
   <?php }?>
        </select>
  </div>
    
      <div class="form-group">
    <label for="exampleInputEmail1">Vehicle price</label>
     <input name="price" type="number" class="form-control" id="price" aria-describedby="emailHelp" placeholder="Vehicle price">
   
  </div>
     <div class="form-group">
    <label for="exampleInputEmail1">Message</label>
    <textarea name="message" id="message"></textarea>
  </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>

<script type="text/javascript">
    function getPriceData(){
        var id=document.getElementById('vehicle').value;
        
        var send_data = {action: 'get_vehicles_price', id: id};
        jQuery.ajax({
        type: 'POST',
        url: '<?php echo admin_url( 'admin-ajax.php' ); ?>',
        data: send_data,
        dataType: 'JSON',
        success: function(result) {
            if(result.price>0)
            document.getElementById('price').value=result.price;
            else
            document.getElementById('price').value=0;
        }
    });
    }
    </script>
  <?php }
   
   public function activationBookingPlugin(){
       global $wpdb;
       $sql = " CREATE TABLE IF NOT EXISTS ".$wpdb->base_prefix."user_booking(
					  id int(11) NOT NULL AUTO_INCREMENT,					 
					  first_name varchar(50) NOT NULL,
					  last_name varchar(50) NOT NULL,
					  email varchar(50) NOT NULL,
					  phone_no varchar(50) NOT NULL,
					  vehicle_type int(11) NOT NULL,
                                          vehicle int(11) NOT NULL,
                                          price double NOT NULL DEFAULT '0',
                                          message text NOT NULL,
                                          booking_status ENUM('Pending', 'Approved', 'Reject','On the way','Complete') NOT NULL,
					  PRIMARY KEY  (id)	    
				)";
       require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta($sql);
   }
   
   
   public function getVehicles(){
       
   }
   
   
   public function addVehiclesMetaboxes(){
       add_meta_box(
		'wpt_events_price',
		'Vehicle Starting Price',
		array($this,'set_vehicle_price'),
		'vehicle',
		'side',
		'default'
	);
   }
   
   public function saveVehiclePrice($post_id, $post){
       
       
  echo $post_type = $post->post_type;
  $new_meta_value =$_POST['vehicle_price'];
   
  if($post_type=='vehicle' && isset($_POST['vehicle_price'])){
  /* Get the meta key. */
    
  $meta_key = 'vehicle_price';

  /* Get the meta value of the custom field key. */
  $meta_value = get_post_meta( $post_id, $meta_key, true );

  
    if($meta_value=='')
    add_post_meta( $post_id, $meta_key, $new_meta_value, true );


  elseif ($meta_value!='')
    update_post_meta( $post_id, $meta_key, $new_meta_value );
  }
   }

   function set_vehicle_price($post){ ?>
       <?php wp_nonce_field( basename( __FILE__ ), 'vehicle_class_nonce' ); ?>

  <p>
   <input class="widefat" type="text" name="vehicle_price" id="vehicle_price" value="<?php echo esc_attr( get_post_meta( $post->ID, 'vehicle_price', true ) ); ?>" size="30" />
  </p>
  <?php }
   public function bookingList() {
        global $wpdb;
        require_once('inc/booking-list.php');
        $bookingList = new Booking_list();
        echo '<div class="wrap"><h2>User Booking List</h2>';
        $bookingList->prepare_items();
        echo '<div class="">
		 <div class="tablenav top">
                    <div class="alignleft actions">
                       
                    <br class="clear">
                </div>';
         
       $bookingList->display();
       echo '</div>';
    }
   
    
    public function getVehiclesPrice(){
        $post_id=$_POST['id'];
         $price=get_post_meta($post_id, 'vehicle_price', true); 
        echo json_encode(['price'=>$price]);
        die();
       
    }
    
    
    
    public function changeBookingStatus(){
       global $wpdb;
       $status_arr=['Pending', 'Approved', 'Reject', 'On the way','Complete'];
       
       $submit_flag=0;
       if(isset($_POST['booking_status'])){
           $submit_flag=1;
           $wpdb->update($wpdb->base_prefix."user_booking",["booking_status"=>$_POST['booking_status']],['id'=>$_REQUEST['id']]);
          $data_for_mail=$wpdb->get_row("select * from ".$wpdb->base_prefix."user_booking where id=".$_REQUEST['id'],ARRAY_A);
          $this->sendEMail($data_for_mail['email'], "Status changed", "Your booking status has been".$_POST['booking_status'], ADMIN_NAME, ADMIN_EMAIL);
           
       }
       if(isset($_REQUEST['action']) && isset($_REQUEST['id'])){
           $data=$wpdb->get_row("select * from ".$wpdb->base_prefix."user_booking where id=".$_REQUEST['id'],ARRAY_A);
           
            echo '<div class="wrap"><h2>User Booking Details</h2>';
           ?>
  <form method="post" action="">
      <?php if($submit_flag){ ?>
     <div id="message" class="updated notice is-dismissible">
				<p><strong>Status updated successfully.</strong></p>					
	<button type="button" class="notice-dismiss"><span class="screen-reader-text">Dismiss this notice.</span></button></div>
       <?php } ?>
        <table>
      <tr><td>First Name</td><td><?php echo $data['first_name']; ?></td></tr>
      <tr><td>Last Name</td><td><?php echo $data['last_name']; ?></td></tr>
      <tr><td>Email</td><td><?php echo $data['email']; ?></td></tr>
      <tr><td>Phone No</td><td><?php echo $data['phone_no']; ?></td></tr>
      <tr><td>Vehicle Type</td><td><?php $tr= get_the_category_by_ID( $data['vehicle_type'] );
      if(!is_object($tr)){
          echo $tr;
      }?></td></tr>
      <tr><td>Vehicle </td><td><?php echo get_the_title($data['vehicle']); ; ?></td></tr>
      <tr><td>Price</td><td><?php echo $data['price']; ?></td></tr>
      <tr><td>Message</td><td><?php echo $data['message']; ?></td></tr>
      <tr><td>Status</td><td><select name="booking_status" id="booking_status">
               <?php
               for($i=0;$i<count($status_arr);$i++){
               ?>
                  <option value="<?php echo $status_arr[$i]; ?>" <?php if($data['booking_status']==$status_arr[$i]){ ?> selected="" <?php } ?>><?php echo $status_arr[$i]; ?></option>
               <?php } ?>
              </select></td></tr>
  </table>
      <input type="submit" value="Update"  class="button">
  </form>
        <?php echo '<div class="">';
        
        echo "</div></div>";
       }
    }

    public function sendEMail($to, $subject, $body, $from_name = '', $from_email = '') {
        
        $headers = array('Content-Type: text/html; charset=UTF-8');
        if ($from_name != '' && $from_email != '') {
            $headers[] = 'From: ' . $from_name . ' <' . $from_email . '>';
        }

        wp_mail($to, $subject, $body, $headers);
    }

}


